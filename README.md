# olkisite

The main site for the OLKi research project. Sub-sites corresponding to
sub-projects might be found in other repositories.

### License

Copyright (C) 2018-2019 LORIA

Code is released under [AGPL-3.0-or-later](http://www.gnu.org/licenses/).

Content in `./content/` and `./data/` is available under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
