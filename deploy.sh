#!/bin/bash

# must be compiled with hugo version,: Hugo Static Site Generator v0.55.6-A5D4C82D linux/amd64 BuildDate: 2019-05-18T07:56:30Z


# rsync -av --delete public/ /var/www/html/olki/
# rsync -av --delete -e "ssh -i /home/gitlab-runner/.ssh/id_rsa" public/ xtof@olkihost.loria.fr:/var/www/html/website/
rsync -av --delete public/ olkihost:/var/www/html/website/
rsync -av xtof/python4nlp.php olkihost:/var/www/html/website/
rm -f pp
touch pp
echo 'AuthType Basic' >> pp
echo 'AuthName "Restricted Content"' >> pp
echo 'AuthUserFile /etc/apache2/.htpasswd' >> pp
echo 'Require valid-user' >> pp
rsync -av pp olkihost:/var/www/html/website/intranet/.htaccess

ssh olkihost 'ln -s /local/olki/video /var/www/html/website/'

