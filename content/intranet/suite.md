---
title: "Suite OLKi"
date: 2019-05-21T12:02:00+06:00
description : "(Only for project members)"
type: post
image: 
author: Christophe Cerisara
tags: ["project"]
---

# Points à améliorer

- Prise de conscience / implication au sein des labo
  - TODO: mieux s'aligner avec la politique scientifique des labos
- Etre plus accueillant pour les chercheurs intéressés
  - budgets ? Groupes de travail ? Un membre-contact ?

# Pistes

- Garder:
  - Interdisciplinarité
  - Ethique

- Ouvrir:
  - A de nouveaux chercheurs venant des labos actuels
  - A de nouveaux labos: chimie ? histoire ? droit ?
  - des industriels ?
  - Construire un objet de travail commun (corpus) ?

- Plateforme fédérée: la poursuivre ? réorienter ?
  - Force: aspect fédéré
  - Faiblesse: effet réseau
  - Pistes de déploiement:
    - Knowledge Alliance (Miguel): reporté
    - Université Européenne (Karl): ?
    - CLARIN: prendra du temps
    - INIST: en support du data management plan: ?
    - Entrepot de donnees UL (Thomas Jounveau)
    - Federated deep learning (François): Waou effect !!!


