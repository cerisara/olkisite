---
title: "What is OLKi ?"
date: 2019-05-21T12:02:00+06:00
description : "La plateforme OLKi"
type: post
image: 
author: Christophe Cerisara
tags: ["project"]
---

OLKi is a research project. An outcome of the project will be a platform, which is:

- For **everybody**, to share resources, any kinds of resources (files, programs, datasets...), and to communicate. You stay in control of your resource, and you benefit from the federation. You don't need to install anything and may choose to be hosted, if you prefer.

<br>
<br>
It's not only for everybody, it's also for scientists, professionals and amateurs; it's

- <a href="../../detpage/socialnet/">A **social network** for scientists, and citizens as well</a>
- <a href="../../detpage/services/">A network that will support **services** for scientists</a>
- <a href="../../detpage/ethics/">A pratical solution towards an **ethical** transformation of science</a>

----

The vision of OLKi is a federation for scientists, i.e. a decentralized network 
potentially composed of many University servers, each one managing its own community.
As it implements the W3C ActivityPub standard, this scientific network
will also be part of the Fediverse, a community-managed multimodal social
network that hosts 2 million citizens, hence enabling direct interactions
between scientists and citizens.

Two services will initially be deployed on OLKi:

1- Resource sharing: every user may upload (under the control of the hosting
node) and share resources (datasets, papers, programs, models, videos...),
or import them from data repositories (such as ORTOLANG, Zenodo, CLARIN,
Dataverse, arXiv, HAL...) with OAIPMH. Note that OLKi does not offer persistent
storage, and thus is not a data repository. OLKi is complementary from data
repositories: it makes their metadata visible on a global decentralized
social network to scientists and citizens; and it handles sharing short-term
resources.

2- Instantaneous scientific communication: beyond traditional conferences,
journals and emails, some scientists have expressed the need to communicate
more quickly on social media (Twitter, Reddit, Researchgate, Academia...). 
OLKi provides open-source solutions for that (including math rendering, referencing
resources...) on a global federated social network already used by citizens,
while keeping all of their data under their control on their local node.

Facilities to easily implement and deploy new services over the OLKi platform
will be provided; future services include:

- Bots for scientific watch and literature review
- Access to NLP tools API hosted on specialized nodes (ORTOLANG...)
- Federated deep learning to enable multi-task transfer learning of AI models between Universities

Compared to most existing solutions, the OLKi vision presents the following two main sets of advantages:

- As a decentralized solution, the costs are shared amongst participating
actors, there is no single point of failure, the network is resilient to
attacks, scalability and sustainability are unlimited, joining the network is
free and open without any control...

- As a federation, the platform is community-managed, transparent; the software
and APIs are open-source; hosting and connection **policies are defined per
node/University**; and the platform is ethical because there is no privileged
node who has more information or control than any other,
and data providers stay in control of their own data on their local node.

<img src="../../images/olkiplat.svg" width="80%" style="display:block; margin-left:auto; margin-right:auto"/>

