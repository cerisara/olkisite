---
title: "Machine Learning on copyrighted data"
date: 2019-11-19T12:02:00+06:00
description : "Machine learning and copyrighted data"
type: draft
image: 
author: Christophe Cerisara
tags: ["AI", "deep learning"]
---

## Training algorithms on copyrighted data is not illegal, according to the United States Supreme Court

According to a recent Supreme Court decision in the USA, it is allowed to
train machine learning models on copyrighted data:

https://towardsdatascience.com/the-most-important-supreme-court-decision-for-data-science-and-machine-learning-44cfc1c1bcaf

This decision is very impacting in the current debate about data ownership and privacy, but it certainly should not
be interpreted in an oversimplistic way, as there is always the problem of re-generating the source data from a number
of recent deep learning models, which is likely to moderate these conclusions in many practical cases...

