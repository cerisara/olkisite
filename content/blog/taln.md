---
title: "TALN workshops"
date: 2020-01-21T12:02:00+06:00
description : "TALN workshops"
type: post
image: 
author: Christophe Cerisara
tags: ["AI"]
---

One workshop and one tutorial linked with OLKi have been accepted at the TALN conference:

- **ETeRNAL**: about ethics and NLP
- **CLARIN-FR**: about language resources and platforms

Follow us on Mastodon ( @admin@olkichat.duckdns.org ) or have a look at the Mastodon stream
on the landing page of [OLKI](https://olki.loria.fr) to get more news in the next few weeks
about these workshops !


