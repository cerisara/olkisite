---
title: "Claire Gardent is granted an AI Chair"
date: 2019-12-14T12:02:00+06:00
description : "Claire Gardent is granted an AI Chair"
type: post
image: 
author: Christophe Cerisara
tags: ["AI"]
---

## Claire Gardent has been granted an AI Chair

Claire Gardent, CNRS researcher in the Synalp research team, is already internationally
recognized for her contributions in Natural Language Processing and Artificial Intelligence.
She has been granted in December 2019 a national "AI Chair": only 3 chairs have been granted in the Région Grand Est.

This is a great news for the OLKi project, and the development of Artificial Intelligence in the region !

<p style="font-size: 0.9rem;font-style: italic;"><img style="display: block; height: 200px;" src="https://svgsilh.com/png/3264733-4caf50.png" alt="wreath"><a href="https://svgsilh.com/ms/4caf50/image/3264733.html">"wreath"</a> is licensed under <a href="https://creativecommons.org/licenses/cc0/1.0/?ref=ccsearch&atype=html" style="margin-right: 5px;">CC0 1.0</a><a href="https://creativecommons.org/licenses/cc0/1.0/?ref=ccsearch&atype=html" target="_blank" rel="noopener noreferrer" style="display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;"><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.creativecommons.org/static/img/cc-cc0_icon.svg" /></a></p>

