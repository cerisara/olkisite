---
title: "Projets 2023 pour preparer OLKi2"
date: 2022-09-21T12:02:00+06:00
description : "Projets 2023 LUE OLKi2"
type: post
image: 
author: Christophe Cerisara
tags: ["OLKi2"]
---

# Projets LUE 2023 pour OLKi2

[texte de l'appel sur la BUL](https://bul.univ-lorraine.fr/index.php/s/ytkCABg6tzExe6p)

### Liste des projets soutenus par OLKi

(L'ordre des projets est totalement aléatoire et ne doit en aucun cas être considéré comme lié d'une
manière ou d'une autre à un ordre préférentiel)

- Karen Fort et Anna Zielinska: éthique de l'IA (AIRed)
- Yannick Parmentier et Mathieu Constant: grammatiseur (EsPaR)
- Julien Falgas et Christophe Cerisara: modèles de langage et Needle (SincerePLM)
- Irina Illina et Céline Ségur: hate speech (Theodesm)
- Maxime Amblard et Jean-Sébastien Rey: Language et bible (Ben Sira Transformation Overtime)
- Alain Polguère et Francesca Ingrosso: vocabulaire de la chimie (T-to-T-Chem)

### Argumentaire

Tous ces projets visent à analyser le discours et le dialogue en contexte sociétal.
Un panel de contextes sociétaux sont choisis: celui des apprenants
(EsPaR), des sachants (Ben Sira Transformation Overtime, T-to-T-Chem) et 
des curieux (AIRed, SincerePLM, Theodesm).
Le dialogue lie tous ces projets et constitue le socle de la future dynamique OLKi2:
dialogue bienveillant entre apprenant et enseignant, dialectique entre pairs scientifiques et
dialogue critique entre curieux
et "prophètes" (au sens des marchands d'illusion).

La dynamique initiée dans OLKi a mené à la continuation des travaux sur le vocabulaire
de la chimie, sur l'éthique de l'IA et sur la détection des discours haineux, qui sont trois
travaux pour lesquels nous avons obtenus dans OLKi des avancées majeures, notamment en terme de publications de qualité.
Les autres projets 
renforceront des pans des travaux de OLKi que nous n'avions pu développer suffisamment, et qui pourtant sont majeurs dans nos sociétés: les apprenants et la formation et les interactions dans les réseaux sociaux.
Ces projets s'inscrivent dans la proposition de Programme Interdisciplinaire LUE
"Langue et société en transition".

