---
title: "Post-doctoral position available"
date: 2020-07-07T12:02:00+06:00
description : "Post-doctoral position available"
type: post
draft: false
image: 
author: Karen Fort
tags: ["ethics"]
---

## JOB DESCRIPTION

This post-doc contract is funded by the EU Horizon2020 Artificial Intelligence for improved PROduction efFICIEncy, quality and maiNTenance (AI-PROFICIENT) project. 

It aims at investigating ethics by design in the context of the industry of the future and more particularly the contribution of digitalization and Artificial Intelligence to predictive maintenance issues and production optimisation including human-system interaction. It is therefore situated at the intersection between machine learning ethics, human-machines and human-systems interactions and data management issues. It represents a unique opportunity to experiment ethics by design in a real human-in-the-loop industrial setting. 

Considering the above paragraphs, the tasks related with the post-doc goals will cover:

- state of the art bibliography and transfert to the manufacturing field;
- design phase: spreading of the ethical concept and issues to all partners, elicitation of  ethical consideration in the use case definition;
- development phase: ensuring that ethical issues are understood and considered by solution developers, foster the relation between developers and users to ensure ethical issues are properly considered;
- in situ evaluation ethics: from design key ethical performance indexes definition to workers situation assessment, in order to address the benefits and stepbacks between the beginning and the implantation of the project.

## THE PROJECT

AI-PROFICIENT aims at bringing advanced AI technologies to manufacturing and process industry, while improving the production planning and execution, and facilitating the collaboration between humans and machines. Taking the full advantage of AI capabilities and human knowledge, AI-PROFICIENT will develop proactive control strategies to improve the manufacturing process over three main vectors: production efficiency, quality and maintenance. AI-PROFICIENT will increase the positive impact of AI technology on the manufacturing process as a whole, while keeping the human in central position, assuming supervisory (human-on-the-loop) and executive (human-in-command) roles. AI-PROFICIENT intends to identify the effective means for human-machine interaction, while respecting the safety and security requirements and following the ethical principles, in order to enable: event identification and prediction, operation scenarios simulation, transparent decision and optimal control, and personalized shop-floor assistance. 
The pilot sites of the project are the CONTINENTAL tyre manufacturing plant in Sarreguemines and the INEOS Olefins & Polymers production sites in Geel (Belgium) and Cologne (Germany).

## REQUIREMENTS

- PhD in philosophy or computer science/AI with a strong experience in ethics, especially ethics by design.
- Candidates with interests in analytical philosophy (e.g. ethics of algorithms, ethics of AI, ethics of technology, philosophy of action) and strong affinity (or degree in) philosophy of science, epistemology, philosophy of technology, philosophy of engineering and computer science are strongly encouraged to apply.
- Strong interests in interdisciplinary research.
- Excellent command of written and spoken English (French not required, but would be a plus).
- Solid knowledge of the GDPR regulation 
- Organizational skills in project management and ability to work in a team.

The post-doc will be integrated in the project and as such will participate in meetings, write deliverables, present progress and results and initiate publications. 

## CONDITIONS OF EMPLOYMENT

- Fixed-term contract: 1 year + 1 year. 
- Salary according to experience (between €2,100 and €2,900 per month before taxes), including the French national health insurance.

## EMPLOYER

The future post-doc will be recruited by Université de Lorraine to work in Nancy, France, at the LORIA laboratory (https://www.loria.fr/en/), a research unit, common to CNRS, the University of Lorraine and INRIA. The lab groups more than 400 researchers, which scientific work is conducted in 28 teams dealing with fundamental and applied research in computer sciences.

## ADDITIONAL INFORMATION

To apply or get more information about this vacancy, you can contact Karën Fort and Christophe Cerisara,
emails: karen.fort@loria.fr and christophe.cerisara@loria.fr

Please include in your application: 

- CV (including a list of publications), 
- motivation letter including names of two references. 

Applications will be considered until October 15, or until the position is filled. 

