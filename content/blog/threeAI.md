---
title: "The 4 meanings of AI"
date: 2019-05-21T12:02:00+06:00
description : "AI Abuse"
type: post
image: 
author: Christophe Cerisara
tags: ["project"]
draft: True
---

The term Artificial Intelligence have multiple meanings and it is important to distinguish them and understand their difference.
The four main meanings of AI are:

- The **historical meaning**: A long time ago, starting from 1980, AI was only a research domain, which aimed at mimicking within machines a few human cognitive process, especially those related to
perception: speech recognition and vision, but also playing games like chess, interacting with the environment with robots and a few other ones.
There were no concrete applications at that time, because the performances were not good enough, and reserchears' interest was mostly targeted at increasing scientific knowledge.
A variety of methods were investigated: formal methods, for instance based on logics or grammars, but also many types of statistical approaches, including machine learninig.
- The **deep learning meaning**: Starting from 2010, deep learning methods have brought impressive improvments in performances in the domain, largely surpassing those of formal methods and traditional
machine learning and making industrialisation of the domain possible. Modern scientists and companies usually refer to the deep learning methods when they talk about AI, which have become so useful
that they are also used in application domains that have nothing to do with the original view of mimicking cognitive processes, but everything to do with processing huge amounts of data,
every kind of data: weather prediction, machine and industrial sensors, satellite, sonar and X-ray images, etc.
- The **GAFAM meaning**:
- The **science fiction meaning**:

