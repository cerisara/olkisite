---
title: "Summer school in NLP"
date: 2019-04-29T12:02:00+06:00
description : "Summer school in Nancy"
type: post
image: 
author: Christophe Cerisara
tags: []
---

We organize a summer school about python programming for Natural Language Processing, in Nancy, in the last week of August.

Please [go there](https://synalp.loria.fr/python4nlp/) for more details !



