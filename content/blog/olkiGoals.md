---
title: "Pourquoi OLKi ?"
date: 2019-04-20T12:02:00+06:00
description : "Constat et objectifs d'OLKi"
type: post
image: 
author: Christophe Cerisara
tags: ["AI", "society"]
---

# Problèmes actuels (constat)

1. Les réseaux sociaux sont contrôlés par les USA et la Chine; c'est un problème pour la recherche européenne car:
    1. Ils contiennent l'information/la connaissance sur le monde la plus riche en langage naturel
    2. La communication scientifique passe de plus en plus par les réseaux sociaux
2. "Data is the new oil" & "AI is the new electricity"
    1. **Importance géostratégique des data** + IA (mais sans data, pas d'IA)
    2. Les acteurs qui contrôlent les data contrôlent les progrès en recherche/science
3. **Méfiance et rejet** grandissants des citoyens vis-à-vis de l'IA
    1. Il faut un autre modèle *véritablement* éthique

# Solutions apportées par OLKi

1. Il faut intégrer aux recherches en IA la question de la gestion des data: nous proposons un modèle de gestion ouvert et équitable
    1. Les producteurs des data contrôlent leur data
    2. Ouverture = équité dans l'accès aux data
    3. API d'accès open-source et communautaire = contrôle partagé, possibilités d'expérimenter illimitées
2. Réflexion interdisciplinaire sur l'éthique
    1. Insuffisance des "grands principes" de toutes les chartes éthiques actuelles, qui s'appuient sur le légal ou la confiance que les citoyens *doivent* avoir
    2. Il ne faut espérer une confiance des citoyens, mais leur donner les moyens de vérifier a posteriori: une licence open-source pour les modèles
    3. Rapprocher citoyens et scientifiques = amener la science vers les citoyens, sur les réseaux sociaux
3. Vers un eco-système de recherche fédéré:
    1. Partage des coûts, extension sans surcoût, résilience...
    2. Emergence des communautés scientifiques et auto-structuration
    3. Intégration dans le réseau fédéré des **data + communication + expérimentation**
    4. Valeur ajoutée:
        1. Contrôle communautaire du réseau, des API (Expérimentations facilitées)
        2. Complémentaire des autres plateformes de stockage des data
        3. Intégration Data + Communication + Expériences (un seul réseau pour tous et toutes les modalités)
        4. Ethique, ouvert

