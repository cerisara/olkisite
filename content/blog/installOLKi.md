---
title: "OLKi platform: quick install guide"
date: 2020-06-25T12:02:00+06:00
description : "OLKi platform: quick install guide"
type: post
draft: false
image: 
author: Christophe Cerisara
tags: ["AI"]
---

## Install the OLKi platform

** A more recent version of this post is available [here](https://members.loria.fr/CCerisara/#olkisocial/) !**

A common situation is the following, where we assume that you have:

- An ubuntu computer with apache + docker
- A personal domain name, say "olki.toto.fr", which will be your entry point to your instance

Then the easiest way to install the platform is to use the latest docker image.

After cloning the OLKi repository, there are 3 configuration files that are important,
as explained [in the official documentation](https://framagit.org/synalp/olki/olki/-/wikis/Admin-documentation): 

- The file "./.env" that contains the OLKi platform configuration
- The file "./support/docker/.env" that contains the config to setup OLKi within docker
- The file "./support/docker/docker-compose.yml" that configure all docker containers together and with the host

You may still need to make the OLKi docker container connect smoothly to your apache web server !
To do so, just add these lines into your apache configuration file:

```
<VirtualHost *:80>
ServerName olki.toto.fr
  RewriteEngine On
  RewriteCond %{HTTP:Upgrade} =websocket [NC]
  RewriteRule /(.*)           ws://127.0.0.1:5000/$1 [P,L]
  ProxyPass / http://127.0.0.1:5000/
  ProxyPassReverse / http://127.0.0.1:5000/
  ProxyRequests Off
  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Then reload this configuration file:

```
sudo systemctl reload apache2
```

Then, you just have to launch the OLKi platform:

```
sudo docker-compose -f support/docker/docker-compose.yml up -d
```

And you may now point any browser to http://olki.toto.fr !

## Troubleshooting

You may launch the docker-compose command without the final "-d" to see in your terminal the logs.

#### "olki" cannot connect to "postgresql"

This is likely due to a configuration problem either in your .env, or in your docker-compose.yml .
Here are a few explanations about how everything should work, to help you identify the erroneous config:

- docker-compose creates an internal network between your containers, and in particular the postgres and olki containers; each container in this network can be accessed by its name, e.g., "olki" and "postgres" (defined in docker-composer.yml)
- the postgres container exposes in this internal network the database on the standard 5432 port
- so the olki container shall access the database at hostname "postgres" and port "5432": this should be defined in your support/docker/.env, which should look like this:

```
DATABASE_URL=postgresql://olki:olki@postgres/olki
CACHE_URL=redis://redis:6379
OLKI_WEBSERVER_HOST=localhost
OLKI_WEBSERVER_PORT=443
OLKI_WEBSERVER_HTTPS=true
# If you need more than one IP as trust_proxy
# pass them as a comma separated array:
OLKI_TRUST_PROXY=["127.0.0.1", "loopback", "172.18.0.0/16"]
#OLKI_SMTP_USERNAME=
#OLKI_SMTP_PASSWORD=
OLKI_SMTP_HOSTNAME=postfix
OLKI_SMTP_PORT=25
OLKI_SMTP_FROM=noreply@domain.tld
OLKI_SMTP_TLS=false
OLKI_SMTP_DISABLE_STARTTLS=false
OLKI_ADMIN_EMAIL=cerisara@loria.fr
```

- In this .env, the first line defines the env variable that olki uses to access the db; the port is not given because it's the standard 5432 port; the "@postgres/" indicates the "hostname" of the container with the db;
  - note that the internal network across containers has IP "172.18.0.X"
  - I think the OLKI_WEBSERVER_* variables are not used any more, but it may be good to have them to avoid deprecated warnings (?)
  - You may expose the platform on http, although for federation https is likely required; it may be possible to still not use https by setting the var EXTERNAL_REQUESTS_VERIFY_SSL to false, but this is not recommended.
  - You don't really need SMTP, as you may always create users using the command-line

- if your .env looks correct, then double check that docker-compose is using **the correct .env file**: this is in the file docker-composer.yml, at the line: "source: ../../.env"

#### How to reset

```
   sudo su
   docker-compose down
   docker rm -f $(docker ps -a -q)
   docker volume rm $(docker volume ls -q)
   docker rmi $(docker images -a -q)
```

