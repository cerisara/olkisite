---
title: "Ethique des usages des données"
date: 2019-04-21T12:02:00+06:00
description : "Vers une charte OLKi"
type: post
image: 
author: Christophe Cerisara
tags: ["AI", "society"]
---

# Réunion ouverte sur l'éthique

Le débat est ouvert vendredi 24 mai, de 16h à 18h, au LORIA, en salle B013 !

Si cela vous intéresse, envoyez-moi un email ou prévenez-moi sur [Mastodon](https://olkichat.duckdns.org/@admin).

# Problématique

Les données sont présentes partout et les mouvements contestataires isolés ("aucun portable chez moi !")
ne résoudront rien; il faut changer de modèle.
De nombreuses chartes et manifestes éthiques sur l'usage des données ont ainsi été proposés au cours de ces dernières années,
et nous proposons d'en faire un recensement partiel et subjectif ci-dessous.
N'hésitez pas à venir en discuter en m'envoyant un message public sur [Mastodon](https://olkichat.duckdns.org/@admin).

Cette revue n'est pas le seul fruit de mon travail, mais elle résulte d'un travail collectif mené dans le cadre d'un
groupe de travail sur l'éthique du projet OLKi, groupe qui n'a pas encore rédigé de rapport, et ce résumé n'est que ma vision personnelle
d'une partie de ce travail.
Cette réflexion est toujours en cours en 2020.

# Revue de l'existant

Le nombre de chartes, manifestes, serments, initiatives qui ont récemment traité de l'éthique des données est impressionnant.

Citons par exemple, du côté des organismes officiels:

* Le [Serment d'Hippocrate pour Data Scientist](https://hippocrate.tech)
* La [Charte éthique et Big Data](http://wiki.ethique-big-data.org/chartes/CharteEthiqueBigDatav8.pdf)
* Les rapports du [COERLE INRIA](https://intranet.inria.fr/Actualite/Le-Coerle-au-caeur-des-questions-d-ethique-et-d-integrite-scientifique)
* Le [groupe éthique IA de l'UE](http://europa.eu/rapid/press-release_IP-19-1893_fr.htm) et ses [guidelines](https://ec.europa.eu/digital-single-market/en/news/ethics-guidelines-trustworthy-ai)
* La [déclaration de Montréal pour une IA responsable](https://www.montrealdeclaration-responsibleai.com/the-declaration)
* https://www.etalab.gouv.fr/algorithmes-publics-des-eleves-de-lena-formulent-une-serie-de-recommandations-sur-les-enjeux-dethique-et-de-responsabilite

Les associations ne sont pas en reste, avec par exemple:

* Les [guidelines de l'association des chercheurs internet](https://aoir.org/ethics)
* Le [collectif Savoirs Com1](http://www.savoirscom1.info)
* Le blog Silex sur [le RGPD](https://scinfolex.com/2018/07/18/donnees-personnelles-et-recherche-scientifique-quelle-articulation-dans-le-rgpd) ou l'affaire [disinfolab](https://scinfolex.com/2018/08/21/affaire-disinfolab-quelles-retombees-potentielles-sur-la-recherche-publique-et-la-science-ouverte)
* Un [manifeste éthique](https://ind.ie/ethical-design) parmi d'autres
* Framasoft, qui publie [demain les nains](https://framablog.org/2019/03/14/demain-les-nains), plaidoyer pour une déconcentration des pouvoirs
* [Wikipedia](https://en.wikipedia.org/wiki/Big_data_ethics) reste une excellente référence et propose d'autres pointeurs
* Les revues académiques comme [The Conversation](http://theconversation.com/pour-des-principes-juridiques-de-responsabilite-adaptes-a-lintelligence-artificielle-115260) publient de nombreux articles sur l'éthique, par exemple [sur le rôle des entreprises](http://theconversation.com/le-role-central-des-entreprises-dans-le-developpement-ethique-de-lia-115256)
* Les [médecins](https://www.youtube.com/watch?v=3JNDox82J4A) s'y intéressent également, en dépassant le cadre des seules données de santé

Enfin, les grandes entreprises, comme Facebook et Google, se positionnent également en injectant par exemple 7.5M€ pour un [centre éthique en IA](https://news.europawire.eu/facebook-to-support-the-new-technical-university-of-munich-tum-institute-for-ethics-in-artificial-intelligence-with-6-5-million-euros-43211021/eu-press-release/2019/01/21/#) en collaboration avec la Technical University of Munich.

# Aller plus loin

Face à ce raz-de-marée d'initiatives éthiques, quel besoin y a-t-il à proposer voire même à réfléchir encore à une nouvelle charte sur l'usage éthique des données ?
Bien au contraire, il ne faut surtout pas laisser les acteurs dominants ou institutionnels seuls maîtres à bord et décider unilatéralement de la "bonne" manière éthique de traiter les données selon eux.
Les menaces pèsent sur les citoyens, et il faut donc les inclure autant que faire se peut dans la réflexion, et lorsque ce n'est pas possible, protéger leur vie privée sans concession.

Ainsi, la grande majorité de toutes les initiatives citées ci-dessus visent à ce que les citoyens "aient confiance" envers les usages qui sont faits de leurs données.
Une telle confiance implique une forme d'abandon: le citoyen laisse faire, car les acteurs qui utilisent ses données ont promis de respecter des principes éthiques.
Je pense au contraire que les citoyens ne doivent pas avoir confiance, et qu'il faut donc leur donner un moyen de contrôle a posteriori, pour qu'ils
puissent vérifier par eux-mêmes qu'il est fait un bon usage de leurs données. 
Un tel contrôle est impossible sans transparence sur les données, les algorithmes et surtout *les modèles* qui sont issus des données.
Il faut donc ouvrir également les modèles, avec des licences open-source appropriées, pour permettre ce contrôle a posteriori.
Bien sûr, tous les citoyens ne réaliseront pas ce contrôle, mais cette possibilité doit rester ouverte, afin que
tous ceux qui en ont la possibilité et l'intérêt, notamment les associations de défense des libertés, puissent réaliser ce contrôle à tout moment.



