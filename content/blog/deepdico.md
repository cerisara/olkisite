---
title: "A deep learning dictionary"
date: 2019-12-13T12:02:00+06:00
description : "A deep learning dictionary"
type: post
image: 
author: Christophe Cerisara
tags: ["AI"]
---

## A deep learning dictionary

[Here](https://frama.link/deepdico) is the first version of a succint, but a bit off-focus, deep learning dictionary.

