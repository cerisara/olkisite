---
title: "Deep Learning for scientists"
date: 2019-03-01T12:02:00+06:00
description : "From introductory to advanced lectures"
type: post
image: 
author: Christophe Cerisara
tags: ["AI", "deep learning"]
---

## The reference

- *The* deep learning [book](http://www.deeplearningbook.org/)

If you want to get into deep learning by studying, the academic way, then you should read this book. It does not include of course
the latest recent developments (such as transformers), but it's still pretty state-of-the-art.

## Video lectures & courses

- [Best series of course into Machine Learning](https://www.youtube.com/watch?v=PlhFWT7vAEw)

At least, these video courses by Nando De Freitas have my preferences because they are quite generic about Machine Learning
(beyond deep learning) and are of very high quality.
This link is just to the first video of the serie, you have to look for all the others [there](https://www.youtube.com/user/ProfNandoDF/videos).

- [Great lectures](https://www.youtube.com/playlist?list=PL3FW7Lu3i5Jsnh1rnUwq_TcylNr7EkRe6) by Stanford Winter 2017

Then, the above lectures are great as well, and they are more specialized into deep learning for Natural Language Processing.

- [Video lectures](https://www.youtube.com/channel/UCPk8m_r6fkUSYmvgCBwq-sw/videos) by A. Karpathy

Then you have these lectures above by A. Karpathy that complete the Standford lectures from the image processing point of view.

- [Older course videos](https://www.youtube.com/channel/UCPk8m_r6fkUSYmvgCBwq-sw/videos)

## Blogs

To get an easy-to-read, but still quality overview of the progress and ideas behind deep learning, I recommend to read good
blogs made by researchers. Here are a few of them, all high quality. Given th quality of these blogs, I do think they are worth scientific
review papers.

- [Major breakthroughs](https://tryolabs.com/blog/2018/12/19/major-advancements-deep-learning-2018) in deep learning, year by year. This link points to 2018 recent progress (BERT, video processing...)

- [Sebastian Ruder's blog](http://ruder.io/deep-learning-optimization-2017/index.html) is a must-read with extremely high quality readings in deep learning, with an applicative focus on NLP, but there is really very general machine learning stuff as well. For example, the link points to a blog about optimization; but you should actually explore all of his posts, tackling many deep learning topics !

- [Lilian's blog](https://lilianweng.github.io/lil-log) is very very good in summarizing within a few pages only all the important material and concepts of deep learning.

## Hands on

There are many tutorials about deep learning on the internet, with enlightening practical programming exercices.
I would recommend the following ones, but they're just my choice:

- A gentle do-it-yourself free online [book](http://neuralnetworksanddeeplearning.com/), on basics about deep neural networks.
- [The pytorch tutorials](https://pytorch.org/tutorials)
- [Keras tutorials](https://github.com/fchollet/keras-resources)
- [Tensorflow tutorials](https://www.tensorflow.org/tutorials)

- Special mention to [Jeremy Fix's Keras tutorials](https://github.com/jeremyfix/deeplearning-lectures), must read ! :-)

## Other resources

- A special topic: machine learning for security, with [lots of resources](https://github.com/jivoi/awesome-ml-for-cybersecurity)
- There are many other video courses, papers, datasets available at [Academic Torrents](http://academictorrents.com) . Go there, look for the keywords that are of interest for you, and enjoy !

