---
title: "ArXiV bot on Mastodon"
type: portfolio
date: 2019-03-20T10:59:54+06:00
description : "Scientific bot delivering you the latest papers"
caption: for social science
image: images/portfolio/publicabot.png
category: ["AI","NLP"]
liveLink: https://miaou.drycat.fr/@publicabot
client: LORIA
submitDate: 2019
location: Nancy
---
### A bot that brings science to your social feed

Social interaction is more present in science than ever, with scientists
sharing and reviewing live on social platforms like Tweeter and Reddit.
However these platforms are poorly tooled nonetheless when it comes to
scientific content. While they will never be fully tooled, we can improve
the situation a little bit by creating tools that can cooperate on such
social platforms, hence the idea of a bot answering basic questions to
give back a list of the latest papers available per topic.

Its name is *publicabot* and you can reach out to it on Mastodon via
https://miaou.drycat.fr/@publicabot.

You can already install it on your own instance, its installation should
be simple and is documented on our repository: https://framagit.org/synalp/olki/publicabot.

You can query it via simple commands: `help` to see usage, `math` or `physics`
(or [any major ArXiV category](http://arxitics.com/help/categories)) to get the latest papers, and that category
followed by a number of papers of your choice to get them all in reverse
chronologic order!

