---
title: "Journée Humanités Numériques & Web Sémantique"
type: portfolio
date: 2021-05-28T10:59:54+06:00
description : "21 juin 2021, LORIA"
caption: HumaNum et WebSem
image: images/portfolio/websem.jpg
category: ["hn"]
client: UL
location: Nancy
---

#### Charte pour l'IA

[Site web](https://poincare.univ-lorraine.fr/fr/manifestations/humanites-numeriques-et-web-semantique)

Le terme d'Humanités Numériques caractérise une communauté et un ensemble de pratiques relatives aux usages numériques pour les Sciences Humaines et Sociales. Ce mouvement traduit une volonté de considérer les nouveaux contenus et médias numériques. Il s'accompagne de nombreuses collaborations entre des chercheuses et chercheurs issus de disciplines variées dont l'un des objectifs est de s'intéresser aux évolutions des pratiques de recherche induites par les possibilités des outils numériques. Dans ce contexte, les technologies du Web sémantique fournissent un cadre pour structurer, lier, explorer et raisonner avec des ensembles de données.

Lors de ces journées, des chercheurs en SHS et des chercheurs menant des travaux autour du Web sémantique discuteront ensemble de ces problématiques. Les interventions mêleront des retours d'expérience, l'introduction de problématiques de recherche ainsi que la présentation d'outils et techniques utilisés par la communauté du Web sémantique.

