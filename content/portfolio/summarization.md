---
title: "Automatic summarization"
type: portfolio
date: 2018-07-12T16:59:54+06:00
description : "Text summarization"
caption: with Seq2seq variants
image: images/portfolio/item-6.jpg
category: ["AI","NLP"]
liveLink: https://synalp.loria.fr
client: Synalp, LORIA
submitDate: 2018
location: Nancy
---
### Research on automatic summarization

The researchers who are mainly active in automatic summarization are:

- Hoa Thien Le (PhD student)
- Guillaume Le Berre (PhD student)
- Claire Gardent
- Christophe Cerisara

We are investigating various variants of seq2seq models, including attention, variational seq2seq, GAN-variational seq2seq,
one-to-many multi-tasking as well as introducing linguistic knowledge in order to generate more abstract and various summaries.

