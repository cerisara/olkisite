---
title: "Charte pour développeurs d'IA"
type: portfolio
date: 2020-07-20T10:59:54+06:00
description : "Charte IA"
caption: Charte IA
image: images/portfolio/charter.jpg
category: ["charte"]
client: UL
location: Nancy
---

#### Charte pour l'IA

Il existe de très nombreuses chartes concernant l'IA, mais
la plupart d'entre elles ne proposent que de grands principes
génériques trop éloignés du quotidien des développeurs d'IA,
ou se focalisent sur certains aspects de l'éthique en IA:
l'éthique scientifique pour garantir des résultats fiables
et honnêtes, la protection des données au sens où elles ne peuvent
échapper au contrôle des entreprises et gouvernements qui les
possèdent, la gestion des biais pour avoir plus d'équité, etc.

La charte que nous proposons ici adopte un point de vue particulier:
protéger les citoyens des conséquences à court ou long terme,
prévisibles ou encore inconnues, des applications d'IA;
aller au-delà des données pour s'intéresser aux modèles, car protéger
les données est loin d'être suffisant, comme nous le montrons dans la charte.

Un grand merci à Maël Pégny pour la recherche et l'écriture de cette charte,
**que vous pouvez consulter [sur HAL](https://hal.archives-ouvertes.fr/hal-03104692) !**

