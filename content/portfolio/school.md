---
title: "NLP summer school"
type: portfolio
date: 2019-04-20T10:59:54+06:00
description : "Python4NLP"
caption: August 2019, Nancy, France
image: images/portfolio/encyc.jpg
category: ["AI","NLP"]
client: LORIA
submitDate: 2019
location: Nancy
---

The OLKi project has joined with the GDR LIFT to propose the
**[Python4NLP Summer school](https://synalp.loria.fr/python4nlp/)**
that has taken place during the last week of August 2019, in Nancy, France.

The week was dedicated to teaching and experimenting with various aspects of
textual corpus processing, from scraping to computing words embeddings.
We further enjoyed two engaging and high quality invited talks, which greatly
contributed to the success of the summer school.

Thank you very much to everyone who joined or help us during this week !

