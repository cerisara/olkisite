---
title: "Digital humanities corpus"
type: portfolio
date: 2018-07-12T16:59:54+06:00
description : ""
caption: Querying a digital humanities corpus
image: images/portfolio/hp.jpg
category: ["NLP"]
liveLink: https://www.univ-lorraine.fr
client: AHP, LORIA
submitDate: 2019
location: Nancy, France
---
### Elastic querying of a corpus of digital humanities corpus

When querying a corpus such as the Henri Poincaré correspondence, the answer to queries is often unsatisfactory though formally correct. Such classical queries can be understood as initial queries, giving a limited set of results. Elastic querying consists in adding to such a query Q a set of transformation rules R associated with explanations that enable to provide new queries, answering related questions that are of interest. The pair (Q, R) is an elastic query whose execution provides a list of triples (r, d, e) ranked by decreasing d, where r is a result (e.g., a letter received or sent by H. Poincaré), d is a matching degree between r and the initial query Q and e explains the mismatch.

This technique of elastic querying can be used in several contexts: search that is neither a crisp search as in classical databases nor an information retrieval search as used in web search engines; assistance with manual indexing of corpus, case-based reasoning (retrieval and adaptation), etc.

This is also meant as a contribution of the representation of notions that are vague or changing, such as the ones that can be used in History.

