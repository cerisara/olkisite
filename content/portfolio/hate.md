---
title: "Hate speech"
type: portfolio
date: 2018-07-12T16:59:54+06:00
description : "Online hate speech and topic classification"
caption: on social networks
image: images/portfolio/hate.jpg
category: ["AI","NLP"]
liveLink: https://www.univ-lorraine.fr
client: CREM, LORIA
submitDate: 2019
location: Nancy-Metz, France
---
### Online hate speech and topic classification

Social networks have rapidly evolved thanks to their trendiness within young people. Nowadays, Twitter, LinkedIn, Facebook and YouTube are used as a very popular tool to communicate ideas, beliefs, feelings or any other form of information. A small percentage of users  employs part of the network for unhealthy activities such as hate speech and terrorism, but the impact of this low percentage of users is significant and very damaging. In September 2018, the French government decided to "move to a new stage" in the fight against racism and hatred on the Internet. 

Within this context and problematic, a PhD project jointly proposed by the Crem and the Loria aims to analyse hate speech in social media and more particularly on Twitter. The study will explore the contexts and topics in which hate speech occurs. This hate speech study will target different protected features such as gender, religion, race, disability, etc.
The scientific challenges of this PhD project are as follows:

- to propose and assess innovative methodological approaches based on deep learning for automatic topic classification of documents containing hate speech; 
- to study and extract knowledge from linguistic data that concern hate speech in social media;
- to better understand hate speech as a social phenomenon, based on the analysed data. 

### Online Hate Speech Against Migrants

According to the 2017 International Migration Report, the number of migrants worldwide has increased rapidly in recent years. This development is causing great public concern around the world, particularly in Europe. The economic crisis affecting some countries of the Old Continent also feeds feelings of insecurity, encouraging the development of anti-immigrant movements. The media are often pointed out for their tendency to depict refugees and migrants negatively, consolidating fears. A recent EU project has revealed a significant increase in hate speech against immigrants and minorities, who are often accused of being the cause of current economic and social problems. Participatory web and social media seem to amplify the intensity and scope of hate speech. The fight against racism and hatred on the Internet is currently one of the priorities of the French government. On September 20th, 2018, a report commissioned on this topic was given to the Prime Minister, containing twenty proposals to combat hate on the Internet. 

A postdoctoral researcher will study the context of the appearance of hateful contents (circumstances of emergence, locutors, dissemination processes, etc.), and to analyze the latter as linguistic productions (narrative approaches, speech acts, enunciation, etc.) in the light of the creation of a lexicon of hate speech in French. The aim is to achieve a better understanding of the social phenomenon of hatred, as well as to improve the development of algorithms used to qualify language.

(more details in http://theconversation.com/combattre-la-haine-sur-internet-trois-defis-a-relever-113385)

