---
title: "Green Chemistry"
type: portfolio
date: 2018-07-12T16:59:54+06:00
description : ""
caption: The Lexicon of Green Chemistry
image: images/portfolio/chem.jpg
category: ["NLP"]
liveLink: https://www.univ-lorraine.fr
client: ATILF, LPCT
submitDate: 2019
location: Nancy-Metz, France
---
### The Lexicon of the Environment and Green Chemistry in Ordinary Discourse. Using Social Networks as Corpora -- LEGCOD

As we live in the time of omnipresent technologies and polluting productions, modern world is facing threatening challenges to *the environment*.
Such problems as greenhouse effect, air and water pollution are high on the agenda at international conferences dealing with global issues. 
As a consequence, environmental domain is no longer regarded as "purely" scientific and ongoing heated debates around the environment reflect 
the continuous upward trend in the public interest. The 1990 Pollution Prevention Act spawned a number of initiatives oriented at achieving 
sustainability. One of them is *green chemistry*, which has been developed as a response to the growing need for reducing environmental 
pollution. Basic principles of this relatively new subdiscipline in chemistry are reflected in a set of twelve rules devised by Paul Anastas and John Warner in 1991.

The main objective of the LEGCOD PhD research is the study of core vocabulary of the fields of the environment and green chemistry, 
with one specific issue in mind: the interplay between scientific/technical usage of this vocabulary -- as specific terminology -- 
and its usage in *ordinary discourse* (general public linguistic productions) found on the Internet, with special attention paid 
to *social networks*.

Social network corpora will be used to study the lexicon of the environment and green chemistry in general language discourse from the 
linguistic point of view. Lexical units do not exist in isolation, they are connected to the rest of the lexicon through semantic and 
combinatorial relations. LEGCOD will draw a lexical map of the environment and green chemistry domains as lexical network: a network of 
lexical units linked by semantic and combinatorial (co-occurrence) relations. Empirical part of the research implies the description of 
the relevant lexical units in the *English and French Lexical Networks*, which are general purpose lexical databases.

