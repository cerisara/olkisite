---
title: "Federated platform"
type: portfolio
date: 2018-07-12T16:59:54+06:00
description : "Scientific fediverse"
caption: for shared science
image: images/portfolio/fedi.jpg
category: ["AI","NLP"]
liveLink: https://fediverse.network
client: LORIA
submitDate: 2019
location: Nancy
---
### For a scientific federated platform

The fediverse is an open-source federated communication platform, which integrates thanks to the ActivityPub protocol
several interconnected modalities, including micro-blogging, blogs, images, music, video, etc.
The most famous "part" of the fediverse is Mastodon, a micro-blogging server controlled by his 2.5 million users.

Our objective is to add another "modality"/"part" into the fediverse dedicated to scientific resources, such as
linguistic corpora, research papers, scientific videos, software tools...

See the current status of this OLKi platform [here](https://olki.loria.fr/platform).

