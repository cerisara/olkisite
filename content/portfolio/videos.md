---
title: "40+ videos liées au projet OLKi"
type: portfolio
date: 2020-07-20T10:59:54+06:00
description : "OLKi videso"
caption: Nos vidéos
image: images/portfolio/peertube.png
category: ["videos"]
client: UL
location: Nancy
---
#### Quelques videos de notre projet

Sur le site des vidéos de l'UL:

- [Collection "IA"](https://videos.univ-lorraine.fr/index.php?act=view&id_col=916)
- [Intro OLKi](https://videos.univ-lorraine.fr/index.php?act=view&id=7537)
- [Peertube](https://videos.univ-lorraine.fr/index.php?act=view&id=7538)
- [Needle](https://videos.univ-lorraine.fr/index.php?act=view&id=7539)
- [Parole et deep learning](https://videos.univ-lorraine.fr/index.php?act=view&id=7540)
- [Extraction de connaissances](https://videos.univ-lorraine.fr/index.php?act=view&id=7542)
- [Intro kickoff](https://videos.univ-lorraine.fr/index.php?act=view&id=7546)
- [Analyse linguistique](https://videos.univ-lorraine.fr/index.php?act=view&id=7548)
- [Présentation de OLKi](https://videos.univ-lorraine.fr/index.php?act=view&id=7630)
- [Philo-info: Alexei Grinbaum](https://videos.univ-lorraine.fr/index.php?act=view&id=9417)
- [Collection: les pratiques journalistiques](https://videos.univ-lorraine.fr/index.php?act=view&id_col=608)
- [Olki à la journée Science ouverte](https://videos.univ-lorraine.fr/index.php?act=view&id=10969)

Sur Youtube:

- [Philo-info: StopCovid](https://www.youtube.com/watch?v=hyjng7aJy0A&feature=youtu.be)
- [Philo-Info: Constantine Sandis](https://www.youtube.com/watch?v=Or1DfNxxNdw)

Sur Peertube:

- [Collection générale](https://videos.ahp-numerique.fr/video-channels/projet_olki/videos)
- [Collection des video-pitch](https://videos.ahp-numerique.fr/videos/watch/playlist/5666f9c8-2aa1-4a19-a03e-2ee4f7b8ba0a?playlistPosition=1)

En local:

- [Qu'est-ce que l'IA ?](https://olki.loria.fr/video/IAdef.mp4)
- [L'IA et le langage](https://olki.loria.fr/video/IAlangage.mp4)

