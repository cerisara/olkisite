---
title: "Towards hate speech detection in all conditions"
type: portfolio
date: 2022-09-01T10:59:54+06:00
description : "Sep 2022, Nancy, France"
caption: Tulika Bose's PhD
image: images/portfolio/tulika.jpg
category: ["NLP"]
client: UL
location: Nancy
---

#### Domain adaptation for hate speech detection

Detecting hate speech is like playing a game of mouse and cat:
while models get better at it, haters find new way to circumvent the models.
Hence, adaptation of these models is important, and in her PhD, Tulika Bose
made remarkable contributions in collaboration between LORIA and Sheffield University
towards this goal...

More info in these papers:

- [https://arxiv.org/pdf/2209.08681.pdf](https://arxiv.org/pdf/2209.08681.pdf)
- [ACL Findings'22](https://arxiv.org/pdf/2203.12536.pdf)
- [SocialNLP'21](https://hal.inria.fr/hal-03204605/file/Social_NLP_NAACL.pdf)

