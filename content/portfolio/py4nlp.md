---
title: "2nd Python4NLP summer school"
type: portfolio
date: 2022-01-09T10:59:54+06:00
description : "9-13 May 2022, Nancy, France"
caption: Python4NLP
image: images/portfolio/py4nlp.jpg
category: ["NLP"]
client: UL
location: Nancy
---

#### 2nd Python4NLP summer school


2nd Python4NLP summer school on collecting, processing and analysing textual data with Python.
The summer school will take place in Nancy, France, from May 9th to May 13th, 2022.
More information on <a href="https://synalp.loria.fr/python4nlp-2022/">https://synalp.loria.fr/python4nlp-2022/</a>.

