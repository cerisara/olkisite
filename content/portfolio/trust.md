---
title: "Workshop on citizen trust and informatics"
type: portfolio
date: 2021-11-09T10:59:54+06:00
description : "11 Nov 2021, online"
caption: Ethics
image: images/portfolio/trust.png
category: ["hn"]
client: UL
location: Nancy
---

#### Workshop on citizen trust and informatics

<a href="https://olki.loria.fr/video/atang.mp4">Video</a>

- Opening conference on citizen trust in Taiwan during the Covid-19 situation by Audrey TANG, Digital Minister of Taiwan

Audrey Tang is a leading figure in East Asian open-source communities and has been Taiwan’s Digital Minister since 2016, an interesting position for a self-identified anarchist who challenges traditional hierarchical structures on an daily basis. A central part of their work has been on making governemnental websites more accessible to all netizens and promoting full transparency of governmental data. They’ve been at the core of vTaiwan, one of the most successful participatory democracy projects ever created which has had tangible impact on Taiwanese policy-making and legislature.

- Session: E-democracy and citizen acceptance of technologies

<a href="https://olki.loria.fr/video/democracy.mp4">Video</a>

Chaire:  Ksenia ERMOSHINA (Centre for Internet and Society)

Véronique CORTIER (LORIA)\\
Anna ZIELINSKA (AHP)\\
David MONNIAUX (VERIMAG)\\

- Session: Trust and technological support systems

<a href="https://olki.loria.fr/video/trust.mp4">Video</a>

Chaire : Enka BLANCHARD, LORIA

Ted SELKER (UMBC Center for Cybersecurity)\\
Asdhley SHEW (Virginia Polytechnic Institute and State University)\\
Eloïse ZEHNDER (2LPN- INRIA)\\

