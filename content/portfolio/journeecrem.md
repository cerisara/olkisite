---
title: "Workshop 19 Nov. 2019 in Metz"
type: portfolio
date: 2019-11-04T10:59:54+06:00
description : "CREM Workshop"
caption: 19th, November 2019, Metz, France
image: images/portfolio/journeeCREM.png
category: ["AI"]
client: CREM
submitDate: 19th Nov. 2019
location: Metz
liveLink: http://crem.univ-lorraine.fr/journalistic-practices-facing-computation-and-automationles-pratiques-journalistiques-face-la
---
### Journalistic Practices Facing Computation and Automation

Until recently, journalists were not concerned by automation threats. Like other creative practices, news writing was thought to profit from computers without running the risk to delegate too much of its value to it. However, advanced computing (or artificial intelligence) is beginning to attract the attention of professionals as well as scholars working on journalism. Algorithms are trained to write articles, built on raw information found on the Internet. Automated social media accounts (newsbots) operate automatic selections of news, contributing to shape their global circulation, and possibly creating echo chambers and communities of readers. Major experiments in automatic news selection, such as Google News, have created opportunities for new business models for the press, challenging the traditional ones. Search engines, especially when they assign importance to news based on digital social ties, influence access to information, and potentially create confining filter bubbles. All these phenomena, and more, require attention of academia and professionals alike. This one-day workshop brings together researchers specialising in these issues to discuss the impacts of automation on journalistic practices, at the crossroads of sociological, technical and ethical considerations.

More information [here](http://crem.univ-lorraine.fr/journalistic-practices-facing-computation-and-automationles-pratiques-journalistiques-face-la)

Videos of the workshop: https://obsweb.net/blog/2019/11/19/automatisation-algorithmes-et-pratiques-journalistiques-quels-enjeux/

