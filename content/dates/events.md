---
title: "Events"
date: 2018-07-15T12:27:38+06:00
description : "(Only for project members)"
type: post
image: 
author: Christophe Cerisara
tags: ["project"]
---

Raw list of past and future meetings, events about OLKi.

<div class="dettable">
<div></div>

DATE        | EVENT
------------|----------------------------------------------------------------
27 sep 2018 | official start of the OLKi project
26 nov 2018 | meeting & decision to co-organize an NLP-AI day with OLKi
29 nov 2018 | presentation of OLKi at the LUE advisory meeting                
6 dec 2018  | meeting: comop OLKi                                              
19 dec 2018 | interview to recruit the OLKi platform engineer                
7 jan 2019  | presentation of OLKi to the Comité des Projets INRIA Grand-Est  
10 jan 2019 | meeting: comop OLKi                                           
14 jan 2019 | presentation of OLKi at the ATILF laboratory                 
15 jan 2019 | interviews post-doc 
17 jan 2019 | presentation of OLKi to the lycée Poincaré Math. Sup. students
24 jan 2019 | presentation of OLKi to the Ecole des Mines students         
29 jan 2019 | presentation of OLKi at the LORIA laboratory                
31 jan 2019 | meeting with LORIA services to setup the OLKi platform infrastructure
5 fev 2019  | presentation of OLKi at the Archives Henri Poincaré laboratory
6 fev 2019  | presentation of OLKi at the AI Meetup in Nancy
7 fev 2019  | meeting: comop OLKi                                           
7 mar 2019  | meeting: comop OLKi                                           
7 mar 2019  | presentation of OLKi at the ISN (teachers of computer science in lycées) meeting
15 mar 2019 | OLKI and AI/NLP day at LORIA (with more than 70 attendants)
21 mar 2019 | paper about "hate speech" in The Conversation (http://theconversation.com/combattre-la-haine-sur-internet-trois-defis-a-relever-113385)
27 mar 2019 | meeting at LORIA to organize an "ethics charte" workshop
28 mar 2019 | presentation of OLKi at "Journées AMI Grand Débat" - CNRS - Paris
1 avr 2019  | meeting: collaboration platforme OLKi w/ [Cenhtor](http://cenhtor.msh-lorraine.fr)
12 avr 2019  | meeting OLKi / OTELo
24 avr 2019  | Ceremony INFRA+
26 avr 2019  | Presentation OLKi at INRIA direction
26 avr 2019  | meeting: comop OLKi
29 avr 2019  | meeting: collaboration ANR w/ ORTOLANG
24 mai 2019  | workshop: ethics
28 mai 2019  | Angeliki Monnier talk
29 mai 2019  | interviews PhD
03 jun 2019  | meeting with dir. doc & edition
05 jun 2019  | meeting: comop OLKi
13 jun 2019  | meeting: platform working group
18 jun 2019  | meeting with Meetup organisers
26 jun 2019  | meeting: comop OLKi
02 jul 2019  | meeting: platform working group
04 jul 2019  | meeting with Numerev
10 jul 2019  | meeting about the future of OLKi
09 aug 2019  | meeting with dir CLARIN
07 sep 2019  | ActivityPub conference in Prague
30 sep 2019  | OLKi at CLARIN conference Bazaar in Leipzig
18 oct 2019  | presentation of OLKi at Telecom hackaton
18 oct 2019  | presentation of OLKi at "nuit des 80 ans du CNRS"
28 jan 2020  | philo-info seminar with Alexei
10 fev 2020  | invitation Cristopher Salvi (Oxford)
17 mar 2020  | conference Jean Lassègue
26 mar 2020  | Ph.D. defense Thien Hoa Le
10 avr 2020  | conference Martin Gibert: "l'éthique de l'IA"
08 jun 2020  | TALN conference
08 sep 2020  | Workshop on social media
08 oct 2020  | Journée Science ouverte à Nancy
05 nov 2020  | Presentation OLKi at ERRSàB (Bordeaux)
30 nov 2020  | Kick-off AI-PROFICIENT project
01 dec 2020  | Successful final review PAPUD project
09 jan 2021  | Publication of the "OLKi charter" [on HAL](https://hal.archives-ouvertes.fr/hal-03104692)

</div>

