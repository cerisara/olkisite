---
title: "Tutoriel CLARIN-FR"
date: 2020-02-06T12:02:00+06:00
description : "Tutoriel CLARIN-FR"
image: 
author: Christophe Cerisara
tags: ["project"]
---

## Tutoriel CLARIN-FR

### Dernières nouvelles (21 mars 2020)

Les actes de la conf JEP-TALN-RECITAL seront publiés, mais la partie "physique" de la conf à Nancy est annulée.
En conséquence, le tutoriel CLARIN-FR, qui ne publie pas d'actes, est annulé.
Nous en sommes désolés, et vous prions de nous en excuser.

Christophe, pour les organisateurs du tutoriel.

---

*Tout ce qui se trouve ci-dessous n'est plus d'actualité: le tutoriel est annulé !*

---

### Objectifs

CLARIN est une infrastructure européenne pour promouvoir une coopération nationale et inter-nationale autour des langues de France.

Un tutoriel CLARIN-FR sera organisé dans le cadre de la conférence [JEP-TALN-RECITAL](https://jep-taln2020.loria.fr/) qui se déroulera à Nancy du 8 au 12 juin 2020.

L'objectif principal de ce tutoriel
est de présenter les usages possibles de CLARIN,
en particulier les ressources principales, les outils et les services proposés par l'infrastructure CLARIN, comme les plateformes de TAL, les centres et les ressources de formation.
Au cours d'une table ronde finale avec les participants, nous parlerons des possibilités de développer de nouvelles actions en rapport avec le monde Francophone.

L'inscription au tutoriel, obligatoire, se fera via le [site de la conférence](https://jep-taln2020.loria.fr/).

### Description du tutoriel

Ce tutoriel présentera, de manière très pratique, les différentes possibilités d'usage de l'infrastructure des services de CLARIN, ainsi que les meilleures manières
d'améliorer la visibilité des ressources langagières francophone (corpora, outils, expertise, etc.).
Les thèmes abordés incluent:

- Comment améliorer la visibilité des ressources francophones dans le CLARIN Virtual Language Observatory;
- Comment utiliser le CLARIN Switchboard, en particulier pour la formation et pour les jeunes chercheurs;
- Comment exploiter les avantages d'une infrastructure fédérée dédiée à la communication autour des ressources;
- Comment partager notre expertise en participant au centre CORLI French K (pour *knowledge*);
- Comment implémenter et utiliser la recherche de contenu fédéré sur vos corpora afin de les rendre plus accessibles;
- Aperçu des outils de TAL francophones

Le tutoriel, d'une durée de 3 heures, sera composé de présentations traditionnelles, d'activités sur le web nécessitant un
ordinateur portable personnel connecté à internet et d'une table ronde.

### Comité d'organisation

- Olivier Baude -Université de Nanterre MODYCO -Nanterre, France
- Patrice Bellot -Aix-Marseille Université CNRS LIS -Marseille, France
- Christophe Cerisara -INRIA LORIA -Nancy, France
- Francesca Frontini -Université Paul Valéry PRAXILING -Montpellier, France -CLARIN Ambassador
- Adeline Joffres -CNRS Huma-Num -Paris, France
- Nicolas Larrousse -CNRS Huma-Num -Paris, France
- Christophe Parisse -Université de Nanterre MODYCO -Nanterre, France

### More information

Des informations plus détaillées sur le tutoriel seront mises en ligne au fur et à mesure sur la page web du tutoriel:

[https://frama.link/tutoclarin](https://frama.link/tutoclarin)
![](../images/tutoclarin.png)

Contacts:

- Nicolas Larousse: Nicolas.Larrousse@huma-num.fr
- Christophe Cerisara: cerisara@loria.fr

Pour recevoir les nouvelles en temps réel, s'abonner à:

- Flux RSS: https://olkichat.duckdns.org/users/jeptaln2020.rss
- Flux ATOM: https://olkichat.duckdns.org/users/jeptaln2020.atom
- Mastodon: https://olkichat.duckdns.org/@jeptaln2020

-----

*Version courte pour diffusion par mail ou via les réseaux sociaux*

Un tutoriel CLARIN-FR sera organisé dans le cadre de la conférence JEP-TALN-RECITAL (https://jep-taln2020.loria.fr/ ) qui se déroulera à Nancy du 8 au 12 juin 2020.
L'inscription au tutoriel, obligatoire, se fera via le site de la conférence.

L'objectif principal de ce tutoriel est de présenter les usages possibles de CLARIN,
qui est une infrastructure européenne pour promouvoir une coopération nationale et inter-nationale autour des langues de France,
en particulier les ressources principales, les outils et les services proposés par l'infrastructure CLARIN, comme les plateformes de TAL, les centres et les ressources de formation.
Au cours d'une table ronde finale avec les participants, nous parlerons des possibilités de développer de nouvelles actions en rapport avec le monde Francophone.

Le tutoriel, d'une durée de 3 heures, sera composé de présentations traditionnelles, d'activités sur le web nécessitant un
ordinateur portable personnel connecté à internet et d'une table ronde.

Des informations plus détaillées sur le tutoriel seront mises en ligne au fur et à mesure sur la page web du tutoriel: https://frama.link/tutoclarin

Comité d'organisation:

- Olivier Baude -Université de Nanterre MODYCO -Nanterre, France
- Patrice Bellot -Aix-Marseille Université CNRS LIS -Marseille, France
- Christophe Cerisara -INRIA LORIA -Nancy, France
- Francesca Frontini -Université Paul Valéry PRAXILING -Montpellier, France -CLARIN Ambassador
- Adeline Joffres -CNRS Huma-Num -Paris, France
- Nicolas Larrousse -CNRS Huma-Num -Paris, France
- Christophe Parisse -Université de Nanterre MODYCO -Nanterre, France

Contacts:

- Nicolas Larousse: Nicolas.Larrousse@huma-num.fr
- Christophe Cerisara: cerisara@loria.fr

