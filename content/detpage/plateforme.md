---
title: "Plateform demo"
date: 2021-11-21T12:02:00+06:00
description : "OLKi platform"
image: 
author: Christophe Cerisara
tags: ["project"]
---

We have maintained a demonstration site for the OLKi plateform prototype
from 2019 to 2021 and we have used it succesfully to demonstrate federation
of corpora across instances, as well as two-way interactions of comments streams
between the platform and the Mastodon social network.
We have also published on our platform video streams that were hosted on a linked
PeerTube server.

This two-years prototype demonstrator has been a success, but we cannot support
anymore its maintenance in the long term, and this is why we have decided to stop
the demonstration as of November 2021.
Indeed, maintaining the demonstrator would require a continuous effort to upgrade
the underlying libraries and make the prototype compatible with their most recent versions and
security patches.

If you are interested, as a company, association or open-source community,
in investing time to maintain or adapt the codebase
to your own application contexts, we would be happy to help you and potentially
participate in this process, please contact us, but as academic research laboratories,
we cannot handle long-term maintenance of this software.

Thank you for your understanding !

