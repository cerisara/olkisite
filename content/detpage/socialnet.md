---
title: "Social network"
date: 2019-05-21T12:02:00+06:00
description : "OLKi platform"
image: 
author: Christophe Cerisara
tags: ["project"]
---

The OLKi platform is a social network for scientists, i.e., a place where scientists can discuss together about
science, about a paper...

Useful features for scientific discussions will be (or are already) implemented:

- Writing maths with a latex-like notation
- Linking to a corpus, or to a part of a corpus
- Communities gather together naturally in the federation paradigm
- Citing a paper
- ...

Thanks to the W3C *ActivityPub* standard, this social network will also be naturally part of the *Fediverse*,
i.e., the federated social network that currently hosts more than 2 millions of citizens.
Hence, scientists and citizens will be naturally connected together.

