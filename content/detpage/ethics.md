---
title: "Towards ethical science"
date: 2019-05-21T12:02:00+06:00
description : "OLKi platform"
image: 
author: Christophe Cerisara
tags: ["project"]
---

The word "Ethics" has many meanings; the one we consider here concerns protecting the privacy of citizens,
making science open and transparent, governing the platform collaboratively, and letting the control of data to data producers.

All these aspects derive from the principles of federation.
As a consequence, the OLKi platform will constitute an ideal solution to free European research from centralized platforms,
and let Europe researchers gain back control over their resources, and progress towards openess and ethics in science.

