---
title: "A network of services"
date: 2019-05-21T12:02:00+06:00
description : "OLKi platform"
image: 
author: Christophe Cerisara
tags: ["project"]
---

The OLKi platform is an ideal framework to deploy scientific services and make them accessible to everyone, while
still being controled by their owner.
The fact to deploy the service within a social network is interesting, as social networks are the places where
people spend most of their time, and more and more expect to find everything they need.

The envisioned services are:

- Services for **scientific watch**: The platform is connected to paper repositories, especially arXiv and HAL. A bot is already running
to post on-demand the latest papers that have been published.
We are working on another bot that is automatically computing a semantic distance between papers to find papers that are the closest from a given text.
- Services for **accessing scientific resources**: The platform may harvest most major resources repositories via OAIPMH. It already supports ORTOLANGand arXiv and plugins shall be written for any other platform (Dataverse, Zenodo, Humanum...). One may also upload his/her own resource, and keep the
control on it.
- Services for **commenting resources**
- Services for **federated deep learning**. Thanks to its implementation of the recent W3C federation standard *ActivityPub*, the OLKi platform is the ideal testbed to experiment federated deep learning algorithms, which transfer the information learned in deep learning models back and forth across the network, hence levereging transfer learning at large scale and enabling even small Universities and partners without much resource and expertise
in deep learning to join the network and build stronger models via automatic collaboration.
- Bibliometric services on HAL

