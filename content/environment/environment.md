---
title: "Environnement scientifique de OLKi"
date: 2021-01-01T12:02:00+06:00
description : "Périmètre scientifique"
type: post
image: 
author: Christophe Cerisara
tags: ["project"]
---

<i>
(The sections of this page are ordered randomly after every refresh of the page)
</i>

<script>
var contents=new Array()

contents[0]=`
<hr>
<h3>Logiciels et modèles de la chimie</h3>

Alexandre Hocquet et Frédéric Wieber, des Archives Henri Poincaré, développent
un programme de recherche sur les logiciels scientifiques, en particulier au
prisme de la science ouverte. Leur objectif est de disposer de corpus
d’archives variés afin d'accéder à différentes facettes de l’histoire de la
chimie computationnelle, en les articulant dans un récit de type biographique
centré sur les logiciels. La thèse centrale défendue est que modèles et
logiciels doivent être traités de concert et que leurs interactions conduisent
à l'idée que la transparence, la validité et la reproductibilité des méthodes
de calcul sont complexes et qu'elles sont une source de tensions pour les
scientifiques. (Wieber et Hocquet, 2020) Afin de mettre à jour et d’analyser
ces tensions, l'archive d'une liste de diffusion académique (Computational
Chemistry mailing List) est utilisée (Hocquet et Wieber, 2018). En localisant
et analysant des « flame wars » dans cette archive, les tensions liées aux
logiciels sont analysées selon des dimensions épistémologiques, de dynamique
disciplinaire et de normes sociales (Hocquet et Wieber, 2017). D'autres corpus
sont également utilisés afin par exemple d’interroger l’évolution de la
politique de licence des logiciels ou celle de leur gouvernance dans ce
domaine.  La position défendue dans ces recherches est que s'intéresser de près
à l’histoire des logiciels scientifiques permet une compréhension plus fine des
enjeux de la science ouverte (Hocquet, 2018).  Que ce soit en chimie
computationnelle, en science du climat (cf le "climategate") ou en
épidémiologie (cf les controverses autour de Neil Ferguson), trop peu
d'attention est portée à l'influence sur la science de la réalisation concrète
des calculs, sur le temps long.  La problématique de l’ouverture de la science,
celle de l’opacité épistémique de ses méthodologies et logiciels, celle de la
reproductibilité computationnelle ont des histoires, qui témoignent du fait que
les contextes où elles se posent sont variés et à prendre en compte.
`

contents[1]=`
<hr>
<h3>De l'humain dans le web sémantique</h3>

Wioletta Miskiewicz, des Archives Henri Poincaré, a poursuivi sa réflexion en faveur du web sémantique basé
sur la compétence des chercheurs et non pas sur l’intelligence artificielle.
Elle défend l’idée que le balisage des représentations numériques des sources
SHS par les chercheurs en SHS, formés dans ce but permettrait aux SHS non
seulement de garder la souveraineté numérique mais donnerait aussi une
représentation numérique plus complexe de nos sources tout en permettant aux
chercheurs de garder un certain contrôle sur ces sources.
`

contents[2]=`
<hr>
<h3>De l'éthique en IA pour l'industrie du futur aussi</h3>
De nombreuses questions liées à la préservation de la vie privée se posent
naturellement également pour la protection des données appartenant aux entreprises,
notamment dans un contexte industriel.
De plus, les modèles d'IA s'invitent de plus en plus au coeur même des industries,
et ces modèles sont souvent fondés sur les mêmes principes que ceux utilisés en
traitement automatique des langues, en particulier en ce qui concerne le deep learning.
C'est pourquoi les membres du projet OLKi collaborent étroitement avec le projet européen
<a href="https://synalp.loria.fr/pages/aiprof/">AI-PROFICIENT</a>.
`

contents[3]=`
<hr>
<h3>Notre voix, menace pour la vie privée</h3>
Tout comme nos empreintes digitales, notre voix peut révéler notre identité.
Elle expose également nos idées, nos convictions, notre colère d'un soir.
C'est une modalité importante en traitement du langage naturel, et particulièrement
sensible aux menaces sur la vie privée.
Le projet européen <a href="https://www.compriseh2020.eu/">COMPRISE</a> propose
de nouvelles solutions pour résoudre ces défis.
`

contents[4]=`
<hr>
<h3>Vers un deep learning explicable</h3>
Comment rendre l'IA compréhensible ?
Une des meilleures approches est certainement d'apprendre à l'IA à générer elle-même des
explications en langage naturel:
Cela sera peut-être bientôt possible grâce au réseau européen <a href="https://synalp.loria.fr/category/projects/nl4xai-h2020-innovation-training-network-itn-project-accepted/">NL4XAI</a>.

`

</script>

<script>
var i=0
//variable used to contain controlled random number 
var random
var spacing="<br>"
//while all of array elements haven't been cycled thru
while (i<contents.length){
    //generate random num between 0 and arraylength-1
    random=Math.floor(Math.random()*contents.length)
    //if element hasn't been marked as "selected"
    if (contents[random]!="selected"){
        document.write(contents[random]+spacing)
        //mark element as selected
        contents[random]="selected"
        i++
    }
}
</script>


